package me.michalziolkowski.resume.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by michal.ziolkowski on 16.11.2017.
 */
public class GeoUtils {

    private static final double RADIUS_OF_EARTH = 6378137.0;

    /**
     * Computes the distance between two coordinates
     *
     * @return distance in meters
     */
    public static double getDistance(LatLng from, LatLng to) {
        return RADIUS_OF_EARTH * Math.toRadians(getArcInRadians(from, to));
    }


    /**
     * Calculates the arc between two points
     * http://en.wikipedia.org/wiki/Haversine_formula
     *
     * @return the arc in degrees
     */
    static double getArcInRadians(LatLng from, LatLng to) {

        double latitudeArc = Math.toRadians(from.latitude - to.latitude);
        double longitudeArc = Math.toRadians(from.longitude - to.longitude);

        double latitudeH = Math.sin(latitudeArc * 0.5);
        latitudeH *= latitudeH;
        double lontitudeH = Math.sin(longitudeArc * 0.5);
        lontitudeH *= lontitudeH;

        double tmp = Math.cos(Math.toRadians(from.latitude))
                * Math.cos(Math.toRadians(to.longitude));
        return Math.toDegrees(2.0 * Math.asin(Math.sqrt(latitudeH + tmp * lontitudeH)));
    }
}
