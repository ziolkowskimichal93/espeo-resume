package me.michalziolkowski.resume.util;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by michal.ziolkowski on 15.11.2017.
 */

public class JsonUtils {

    /**
     * Function returns json string read from given file
     */
    public static String readJSONFromAsset(Context context, String filename) {
    String json = null;
    try {
        InputStream is = context.getAssets().open(filename);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        json = new String(buffer, "UTF-8");
    } catch (IOException ex) {
        ex.printStackTrace();
        return null;
    }
    return json;
}
}
