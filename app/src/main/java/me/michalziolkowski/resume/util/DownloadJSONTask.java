package me.michalziolkowski.resume.util;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by michal.ziolkowski on 15.11.2017.
 */
public class DownloadJSONTask extends AsyncTask<String, String, String> {

    private static final String LOG_TAG = DownloadJSONTask.class.getSimpleName();

    private final DownloadJSONTaskListener listener;

    public interface DownloadJSONTaskListener {
        void onDownloadStart();
        void onDownloadSuccess(String json);
        void onDownloadError();
    }

    public DownloadJSONTask(DownloadJSONTaskListener listener) {
        this.listener = listener;
    }

    /**
     * Notify listener before starting the download
     * */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onDownloadStart();
    }

    /**
     * Downloading json string in background thread
     * */
    @Override
    protected String doInBackground(String... f_url) {
        try {
            URL url = new URL(f_url[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            InputStream input = new BufferedInputStream(url.openStream(), 8192);
            return inputStreamToString(input);
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        return null;
    }

    /**
     * After completing background task return json string
     * **/
    @Override
    protected void onPostExecute(String json) {
        if(json == null){
            listener.onDownloadError();
            return;
        }
        listener.onDownloadSuccess(json);
    }

    private String inputStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            is.close();
        }
        catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage());
            return null;
        }
        return stringBuilder.toString();
    }
}