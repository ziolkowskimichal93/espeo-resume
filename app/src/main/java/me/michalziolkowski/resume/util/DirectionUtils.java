package me.michalziolkowski.resume.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by michal.ziolkowski on 16.11.2017.
 */
public class DirectionUtils {

    /**
     * Function returns google directions api url for origin and destination location
     */
    public static String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    /**
     * Function returns formatted text for given distance in meters
     */
    public static String getDistanceText(double distnaceInMeters){
        if(distnaceInMeters < 1000){
            return String.format("%d m", (int) (distnaceInMeters));
        }else{
            return String.format("%.1f km", (distnaceInMeters/1000));
        }
    }


}
