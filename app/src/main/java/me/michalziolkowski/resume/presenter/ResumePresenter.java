package me.michalziolkowski.resume.presenter;

import android.content.Context;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import javax.inject.Inject;

import me.michalziolkowski.resume.ResumeApplication;
import me.michalziolkowski.resume.model.Resume;
import me.michalziolkowski.resume.view.ResumeView;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */

public class ResumePresenter extends MvpBasePresenter<ResumeView> {

    @Inject Resume resume;

    public ResumePresenter(Context applicationContext) {
        ((ResumeApplication) applicationContext).getResumePresenterComponent().inject(this);
    }

    public void callApplicant() {
        getView().callPhone(resume.getPhoneNum());
    }

    public void loadResume() {
        getView().setToolbarTitle(resume.getName());
        getView().setAboutMeText(resume.getAboutMe());
        getView().setAvatar(resume.getAvatarUrl());
        getView().setProjects(resume.getProjects());
        getView().setEducationList(resume.getEducationList());
        getView().setHobbies(resume.getHobbies());
    }

    public void onNavigationClick() {
        getView().openNavigationActivity();
    }
}
