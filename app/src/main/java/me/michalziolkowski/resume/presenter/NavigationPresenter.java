package me.michalziolkowski.resume.presenter;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import javax.inject.Inject;

import me.michalziolkowski.resume.ResumeApplication;
import me.michalziolkowski.resume.direction.ParserTask;
import me.michalziolkowski.resume.model.Location;
import me.michalziolkowski.resume.util.DirectionUtils;
import me.michalziolkowski.resume.util.DownloadJSONTask;
import me.michalziolkowski.resume.util.GeoUtils;
import me.michalziolkowski.resume.view.NavigationView;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */
public class NavigationPresenter extends MvpBasePresenter<NavigationView> {

    private static final String LOG_TAG = NavigationPresenter.class.getSimpleName();

    private static final String ESPEO_LOCATION_JSON_URL = "https://pastebin.com/raw/uYCM5u0P";
    private static final long ROUTE_UPDATE_INTERVAL = 10 * 1000;

    private long routeUpdatedTimestamp = 0;
    private Location targetLocation;
    private LatLng userLatLng;
    private boolean firstZoom = true;

    @Inject Gson gson;

    public NavigationPresenter(Context applicationContext) {
        ((ResumeApplication) applicationContext).getNavigatePresenterComponent().inject(this);
    }

    public void onMapReady() {
        new DownloadJSONTask(targetLocationDownloadListener).execute(ESPEO_LOCATION_JSON_URL);
    }

    private void updateNavigation() {
        if(targetLocation == null || userLatLng == null) return;

        LatLng targetLatLng = new LatLng(targetLocation.getLatitude(), targetLocation.getLongitude());
        double distance = GeoUtils.getDistance(userLatLng, targetLatLng);
        getView().updateDistance(DirectionUtils.getDistanceText(distance));

        if(firstZoom) {
            zoomToRoute();
        }

        if(shouldUpdateRoute()) {
            Log.d(LOG_TAG, "Updating route directions");
            new DownloadJSONTask(routeDownloadListener).execute(DirectionUtils.getDirectionsUrl(userLatLng, targetLatLng));
            routeUpdatedTimestamp = System.currentTimeMillis();
        }
    }

    private void zoomToRoute() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(targetLocation.getLatitude(), targetLocation.getLongitude()));
        builder.include(userLatLng);
        LatLngBounds bounds = builder.build();
        getView().updateCameraBounds(bounds);
        firstZoom = false;
    }

    private boolean shouldUpdateRoute() {
        return routeUpdatedTimestamp == 0 || System.currentTimeMillis() - routeUpdatedTimestamp > ROUTE_UPDATE_INTERVAL;
    }

    public void updateLocation(LatLng latLng) {
        userLatLng = latLng;
        updateNavigation();
    }

    private DownloadJSONTask.DownloadJSONTaskListener targetLocationDownloadListener = new DownloadJSONTask.DownloadJSONTaskListener() {

        @Override
        public void onDownloadStart() {

        }

        @Override
        public void onDownloadSuccess(String jsonString) {
            targetLocation = gson.fromJson(jsonString, Location.class);
//            targetLocation = new Location(53.421089, 14.540850, "TestLocation");
            getView().showMarker(new LatLng(targetLocation.getLatitude(), targetLocation.getLongitude()), targetLocation.getName());
            updateNavigation();
        }

        @Override
        public void onDownloadError() {

        }
    };

    private DownloadJSONTask.DownloadJSONTaskListener routeDownloadListener = new DownloadJSONTask.DownloadJSONTaskListener() {

        @Override
        public void onDownloadStart() {

        }

        @Override
        public void onDownloadSuccess(String jsonString) {
            new ParserTask(routeParserListener).execute(jsonString);
            Log.d(LOG_TAG, "Root downloaded in " + (int) ((System.currentTimeMillis() - routeUpdatedTimestamp)/1000));
        }

        @Override
        public void onDownloadError() {

        }
    };

    private ParserTask.ParserListener routeParserListener = new ParserTask.ParserListener() {
        @Override
        public void onParsingFinish(List<LatLng> routePoints) {
            getView().showPolyline(routePoints);
            Log.d(LOG_TAG, "and parsed in " + (int) ((System.currentTimeMillis() - routeUpdatedTimestamp)/1000));
        }

        @Override
        public void onParsingError() {

        }
    };
}
