package me.michalziolkowski.resume.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import me.michalziolkowski.resume.model.Project;
import me.michalziolkowski.resume.view.ProjectView;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */

public class ProjectPresenter extends MvpBasePresenter<ProjectView> {

    private Project project;

    public ProjectPresenter() {
    }

    public void setProject(Project project) {
        this.project = project;
        getView().setToolbarTitle(project.getName());
        getView().setProjectName(project.getName());
        getView().setProjectDescription(project.getDescription());
        getView().setProjectImage(project.getLogoUrl());
        getView().setProjectSkills(project.getSkills());
        getView().setProjectScreenshots(project.getScreenshots());
    }
}
