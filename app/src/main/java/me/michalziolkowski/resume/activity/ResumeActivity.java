package me.michalziolkowski.resume.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.ResumeApplication;
import me.michalziolkowski.resume.adapter.EducationAdapter;
import me.michalziolkowski.resume.adapter.HobbiesAdapter;
import me.michalziolkowski.resume.adapter.ProjectsAdapter;
import me.michalziolkowski.resume.model.Education;
import me.michalziolkowski.resume.model.Hobby;
import me.michalziolkowski.resume.model.Project;
import me.michalziolkowski.resume.presenter.ResumePresenter;
import me.michalziolkowski.resume.recycler.RecyclerViewListMargin;
import me.michalziolkowski.resume.util.BitmapUtils;
import me.michalziolkowski.resume.view.ResumeView;

public class ResumeActivity extends MvpActivity<ResumeView, ResumePresenter> implements ResumeView {

    private static final int PROJECTS_GRID_COLUMNS_NUM = 2;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.about_me_text)
    TextView aboutMe;

    @BindView(R.id.projects_grid)
    RecyclerView projectsGridView;

    @BindView(R.id.education_list)
    RecyclerView educationListView;

    @BindView(R.id.hobbies_list)
    RecyclerView hobbiesListView;

    @BindView(R.id.avatar)
    CircleImageView avatar;

    @OnClick(R.id.phone_fab)
    void onPhoneClick() {
        presenter.callApplicant();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        projectsGridView.setLayoutManager(new GridLayoutManager(this, PROJECTS_GRID_COLUMNS_NUM));
        ViewCompat.setNestedScrollingEnabled(projectsGridView, false);

        educationListView.setLayoutManager(new LinearLayoutManager(this));
        educationListView.addItemDecoration(new RecyclerViewListMargin(
                getResources().getDimensionPixelSize(R.dimen.list_item_margin), RecyclerViewListMargin.VERTICAL));
        ViewCompat.setNestedScrollingEnabled(educationListView, false);

        hobbiesListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        hobbiesListView.addItemDecoration(new RecyclerViewListMargin(
                getResources().getDimensionPixelSize(R.dimen.list_item_margin), RecyclerViewListMargin.HORIZONTAL));
        ViewCompat.setNestedScrollingEnabled(hobbiesListView, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadResume();
    }

    @Override
    public ResumePresenter createPresenter() {
        return new ResumePresenter(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.navigation:
                presenter.onNavigationClick();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    @Override
    public void setAvatar(String avatarUrl) {
        avatar.setImageBitmap(BitmapUtils.getBitmapFromAsset(this, avatarUrl));
    }

    @Override
    public void setAboutMeText(String text) {
        aboutMe.setText(text);
    }

    @Override
    public void setProjects(List<Project> projects) {
        projectsGridView.setAdapter(new ProjectsAdapter(this, projects));
    }

    @Override
    public void setEducationList(List<Education> educationList) {
        educationListView.setAdapter(new EducationAdapter(this, educationList));
    }

    @Override
    public void setHobbies(List<Hobby> hobbies) {
        hobbiesListView.setAdapter(new HobbiesAdapter(this, hobbies));
    }

    @Override
    public void callPhone(String number) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number.trim()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestCallPermission();
            return;
        }
        startActivity(intent);
    }

    @Override
    public void openNavigationActivity() {
        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestCallPermission() {
        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.callApplicant();
                }
            }
        }
    }
}
