package me.michalziolkowski.resume.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.ResumeApplication;
import me.michalziolkowski.resume.adapter.ScreenshotsAdapter;
import me.michalziolkowski.resume.adapter.SkillsAdapter;
import me.michalziolkowski.resume.model.Project;
import me.michalziolkowski.resume.model.Screenshot;
import me.michalziolkowski.resume.model.Skill;
import me.michalziolkowski.resume.presenter.ProjectPresenter;
import me.michalziolkowski.resume.util.BitmapUtils;
import me.michalziolkowski.resume.view.ProjectView;

public class ProjectActivity extends MvpActivity<ProjectView, ProjectPresenter> implements ProjectView {

    public static final String PROJECT_PARCELABLE_KEY = "PROJECT";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.project_name)
    TextView projectName;

    @BindView(R.id.project_description)
    TextView projectDescription;

    @BindView(R.id.project_image)
    ImageView projectImage;

    @BindView(R.id.project_skills)
    RecyclerView projectSkills;

    @BindView(R.id.project_screenshots)
    RecyclerView projectScreenshots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        projectSkills.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL));
        ViewCompat.setNestedScrollingEnabled(projectSkills, false);

        projectScreenshots.setLayoutManager(new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL));
        ViewCompat.setNestedScrollingEnabled(projectScreenshots, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Project project = getIntent().getParcelableExtra(PROJECT_PARCELABLE_KEY);
        presenter.setProject(project);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        NavUtils.navigateUpFromSameTask(this);
        return true;
    }

    @NonNull
    @Override
    public ProjectPresenter createPresenter() {
        return new ProjectPresenter();
    }

    @Override
    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    @Override
    public void setProjectName(String name) {
        projectName.setText(name);
    }

    @Override
    public void setProjectDescription(String description) {
        projectDescription.setText(description);
    }

    @Override
    public void setProjectImage(String imageUrl) {
        projectImage.setImageBitmap(BitmapUtils.getBitmapFromAsset(this, imageUrl));
    }

    @Override
    public void setProjectSkills(List<Skill> skills) {
        projectSkills.setAdapter(new SkillsAdapter(skills));
    }

    @Override
    public void setProjectScreenshots(List<Screenshot> screenshotUrls) {
        projectScreenshots.setAdapter(new ScreenshotsAdapter(this, screenshotUrls));
    }
}
