package me.michalziolkowski.resume.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.List;

import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.activity.ProjectActivity;
import me.michalziolkowski.resume.model.Project;
import me.michalziolkowski.resume.util.BitmapUtils;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ImageButtonHolder> {

    private Context context;
    private List<Project> projects;

    public ProjectsAdapter(Context context, List<Project> projects) {
        this.context = context;
        this.projects = projects;
    }

    @Override
    public ImageButtonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_item, parent, false);
        return new ImageButtonHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageButtonHolder holder, final int position) {
        final Project project = projects.get(position);
        holder.imageButton.setImageBitmap(BitmapUtils.getBitmapFromAsset(context, project.getLogoUrl()));
        holder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, ProjectActivity.class);
                intent.putExtra(ProjectActivity.PROJECT_PARCELABLE_KEY, project);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
      return projects.size();
    }

    class ImageButtonHolder extends RecyclerView.ViewHolder {
        ImageButton imageButton;

        ImageButtonHolder(View itemView) {
            super(itemView);
            imageButton = (ImageButton) itemView.findViewById(R.id.project_button);
        }
    }
}