package me.michalziolkowski.resume.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.model.Screenshot;
import me.michalziolkowski.resume.util.BitmapUtils;

public class ScreenshotsAdapter extends RecyclerView.Adapter<ScreenshotsAdapter.ImageButtonHolder> {

    private Context context;
    private List<Screenshot> screenshotUrls;

    public ScreenshotsAdapter(Context context, List<Screenshot> screenshotUrls) {
        this.context = context;
        this.screenshotUrls = screenshotUrls;
    }

    @Override
    public ImageButtonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.screenshot_item, parent, false);
        return new ImageButtonHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageButtonHolder holder, final int position) {
        final String screenshotUrl = screenshotUrls.get(position).getUrl();
        holder.screenshotImage.setImageBitmap(BitmapUtils.getBitmapFromAsset(context, screenshotUrl));
    }

    @Override
    public int getItemCount() {
        return screenshotUrls.size();
    }

    class ImageButtonHolder extends RecyclerView.ViewHolder {
        ImageView screenshotImage;

        ImageButtonHolder(View itemView) {
          super(itemView);
          screenshotImage = (ImageView) itemView.findViewById(R.id.screenshot_image);
        }
    }
}