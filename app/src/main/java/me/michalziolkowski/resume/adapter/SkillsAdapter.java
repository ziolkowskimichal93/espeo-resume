package me.michalziolkowski.resume.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.model.Skill;

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.ImageButtonHolder> {

    private List<Skill> skills;

    public SkillsAdapter(List<Skill> skills) {
      this.skills = skills;
    }

    @Override
    public ImageButtonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.skill_item, parent, false);
        return new ImageButtonHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageButtonHolder holder, final int position) {
        holder.skillName.setText(skills.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return skills.size();
    }

    class ImageButtonHolder extends RecyclerView.ViewHolder {
        TextView skillName;

        ImageButtonHolder(View itemView) {
          super(itemView);
          skillName = (TextView) itemView.findViewById(R.id.skill_name);
        }
    }
}