package me.michalziolkowski.resume.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.model.Education;
import me.michalziolkowski.resume.util.BitmapUtils;

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.EducationItemHolder> {

    private Context context;
    private List<Education> educationList;

    public EducationAdapter(Context context, List<Education> educationList) {
        this.context = context;
        this.educationList = educationList;
    }

    @Override
    public EducationItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.education_item, parent, false);
        return new EducationItemHolder(view);
    }

    @Override
    public void onBindViewHolder(final EducationItemHolder holder, final int position) {
        final Education education = educationList.get(position);

        holder.educationLogo.setImageBitmap(BitmapUtils.getBitmapFromAsset(context, education.getLogoUrl()));
        holder.educationName.setText(education.getSchoolName());
        holder.educationDetails.setText(education.getDetails());
    }

    @Override
    public int getItemCount() {
        return educationList.size();
    }

    class EducationItemHolder extends RecyclerView.ViewHolder {

        CircleImageView educationLogo;
        TextView educationName;
        TextView educationDetails;

        EducationItemHolder(View itemView) {
          super(itemView);
          educationLogo = (CircleImageView) itemView.findViewById(R.id.education_logo);
          educationName = (TextView) itemView.findViewById(R.id.education_name);
          educationDetails = (TextView) itemView.findViewById(R.id.education_details);
        }
    }
}