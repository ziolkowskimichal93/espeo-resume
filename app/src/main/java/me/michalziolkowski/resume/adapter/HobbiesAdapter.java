package me.michalziolkowski.resume.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import me.michalziolkowski.resume.R;
import me.michalziolkowski.resume.model.Hobby;
import me.michalziolkowski.resume.util.BitmapUtils;

public class HobbiesAdapter extends RecyclerView.Adapter<HobbiesAdapter.ImageButtonHolder> {

    private Context context;
    private List<Hobby> hobbies;

    public HobbiesAdapter(Context context, List<Hobby> hobbies) {
        this.context = context;
        this.hobbies = hobbies;
    }

    @Override
    public ImageButtonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hobby_item, parent, false);
        return new ImageButtonHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageButtonHolder holder, final int position) {
        final Hobby hobby = hobbies.get(position);

        holder.hobbyIcon.setImageBitmap(BitmapUtils.getBitmapFromAsset(context, hobby.getIconUrl()));
        holder.hobbyName.setText(hobby.getName());
    }

    @Override
    public int getItemCount() {
        return hobbies.size();
    }

    class ImageButtonHolder extends RecyclerView.ViewHolder {
        ImageView hobbyIcon;
        TextView hobbyName;

        ImageButtonHolder(View itemView) {
            super(itemView);
            hobbyIcon = (ImageView) itemView.findViewById(R.id.hobby_logo);
            hobbyName = (TextView) itemView.findViewById(R.id.hobby_name);
        }
    }
}