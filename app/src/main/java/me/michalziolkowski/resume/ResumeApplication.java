package me.michalziolkowski.resume;

import android.app.Application;

import me.michalziolkowski.resume.di.component.DaggerNavigatePresenterComponent;
import me.michalziolkowski.resume.di.component.DaggerResumePresenterComponent;
import me.michalziolkowski.resume.di.component.NavigatePresenterComponent;
import me.michalziolkowski.resume.di.component.ResumePresenterComponent;
import me.michalziolkowski.resume.di.module.ApplicationModule;
import me.michalziolkowski.resume.di.module.GsonModule;
import me.michalziolkowski.resume.di.module.ResumeModule;

/**
 * Created by michal.ziolkowski on 16.11.2017.
 */
public class ResumeApplication extends Application {

    private ResumePresenterComponent resumePresenterComponent;
    private NavigatePresenterComponent navigatePresenterComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        ApplicationModule applicationModule = new ApplicationModule(this);
        GsonModule gsonModule = new GsonModule();
        resumePresenterComponent = DaggerResumePresenterComponent
                .builder()
                .gsonModule(gsonModule)
                .applicationModule(applicationModule)
                .build();
        navigatePresenterComponent = DaggerNavigatePresenterComponent
                .builder()
                .gsonModule(gsonModule)
                .build();
    }

    public ResumePresenterComponent getResumePresenterComponent(){
        return resumePresenterComponent;
    }

    public NavigatePresenterComponent getNavigatePresenterComponent(){
        return navigatePresenterComponent;
    }
}
