package me.michalziolkowski.resume.view;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */

public interface NavigationView extends MvpView {

    void showMarker(LatLng location, String name);
    void updateCameraBounds(LatLngBounds bounds);
    void showPolyline(List<LatLng> routePoints);
    void updateDistance(String distance);
}
