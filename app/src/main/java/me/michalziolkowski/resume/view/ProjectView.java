package me.michalziolkowski.resume.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import me.michalziolkowski.resume.model.Screenshot;
import me.michalziolkowski.resume.model.Skill;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */

public interface ProjectView extends MvpView {

    void setToolbarTitle(String title);
    void setProjectName(String name);
    void setProjectDescription(String description);
    void setProjectImage(String imageUrl);
    void setProjectSkills(List<Skill> skills);
    void setProjectScreenshots(List<Screenshot> screenshotUrls);
}
