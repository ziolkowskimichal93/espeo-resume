package me.michalziolkowski.resume.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import me.michalziolkowski.resume.model.Education;
import me.michalziolkowski.resume.model.Hobby;
import me.michalziolkowski.resume.model.Project;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */

public interface ResumeView extends MvpView {

    void setToolbarTitle(String title);
    void setAvatar(String avatarUrl);
    void setAboutMeText(String text);
    void setProjects(List<Project> projects);
    void setEducationList(List<Education> educationList);
    void setHobbies(List<Hobby> hobbies);
    void callPhone(String number);
    void openNavigationActivity();
}
