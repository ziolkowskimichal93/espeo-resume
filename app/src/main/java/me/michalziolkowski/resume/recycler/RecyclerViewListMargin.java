package me.michalziolkowski.resume.recycler;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by michal.ziolkowski on 15.11.2017.
 */
public class RecyclerViewListMargin extends RecyclerView.ItemDecoration {

    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    private int orientation;
    private int margin;

    /**
     * constructor
     * @param margin desirable margin size in px between the views in the recyclerView
     */
    public RecyclerViewListMargin(int margin, int orientation) {
        this.margin = margin;
        this.orientation = orientation;

    }

    /**
     * Set right margins for the items inside the recyclerView except for the last item
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildLayoutPosition(view);
        if (position != (parent.getAdapter().getItemCount() - 1)) {
            if(orientation == HORIZONTAL) {
                outRect.right = margin;
            }else if(orientation == VERTICAL){
                outRect.bottom = margin;
            }
        }
    }
}