package me.michalziolkowski.resume.di.component;

import dagger.Component;
import me.michalziolkowski.resume.di.module.ApplicationModule;
import me.michalziolkowski.resume.di.module.GsonModule;
import me.michalziolkowski.resume.di.module.ResumeModule;
import me.michalziolkowski.resume.presenter.NavigationPresenter;
import me.michalziolkowski.resume.presenter.ResumePresenter;

@Component(modules = {GsonModule.class})
public interface NavigatePresenterComponent {

    void inject(NavigationPresenter navigationPresenter);

}