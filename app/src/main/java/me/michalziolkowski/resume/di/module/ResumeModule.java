package me.michalziolkowski.resume.di.module;

import android.content.Context;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import me.michalziolkowski.resume.model.Resume;
import me.michalziolkowski.resume.util.JsonUtils;

/**
 * Created by michal.ziolkowski on 16.11.2017.
 */

@Module
public class ResumeModule {

    private static final String RESUME_JSON_FILENAME = "michal_ziolkowski_resume.json";

    @Provides
    Resume provideResume(Gson gson, String resumeJson) {
        return gson.fromJson(resumeJson, Resume.class);
    }

    @Provides
    String provideResumeJson(Context context) {
        return JsonUtils.readJSONFromAsset(context, RESUME_JSON_FILENAME);
    }
}
