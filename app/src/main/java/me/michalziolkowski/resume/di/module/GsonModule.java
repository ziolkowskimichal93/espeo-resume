package me.michalziolkowski.resume.di.module;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;

/**
 * Created by michal.ziolkowski on 16.11.2017.
 */

@Module
public class GsonModule {

    @Provides
    Gson provideGson() {
        return new Gson();
    }
}
