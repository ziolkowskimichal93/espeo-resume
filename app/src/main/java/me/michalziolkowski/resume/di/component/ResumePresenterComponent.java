package me.michalziolkowski.resume.di.component;

import dagger.Component;
import me.michalziolkowski.resume.di.module.ApplicationModule;
import me.michalziolkowski.resume.di.module.GsonModule;
import me.michalziolkowski.resume.di.module.ResumeModule;
import me.michalziolkowski.resume.presenter.ResumePresenter;

@Component(modules = {ApplicationModule.class, GsonModule.class, ResumeModule.class})
public interface ResumePresenterComponent {

    void inject(ResumePresenter resumePresenter);

}