package me.michalziolkowski.resume.model;

/**
 * Created by michal.ziolkowski on 13.11.2017.
 */

public class Education {

    private String schoolName;
    private String details;
    private String logoUrl;

    public Education(String schoolName, String details, String logoUrl) {
        this.schoolName = schoolName;
        this.details = details;
        this.logoUrl = logoUrl;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getDetails() {
        return details;
    }

    public String getLogoUrl() {
        return logoUrl;
    }
}
