package me.michalziolkowski.resume.model;

import java.util.List;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */
public class Resume {

    private String name;
    private String aboutMe;
    private String avatarUrl;
    private String phoneNum;
    private List<Project> projects;
    private List<Education> educationList;
    private List<Hobby> hobbies;

    public Resume(String name, String aboutMe, String avatarUrl, String phoneNum, List<Project> projects, List<Education> educationList, List<Hobby> hobbies) {
        this.name = name;
        this.aboutMe = aboutMe;
        this.avatarUrl = avatarUrl;
        this.phoneNum = phoneNum;
        this.projects = projects;
        this.educationList = educationList;
        this.hobbies = hobbies;
    }

    public String getName() {
        return name;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public List<Hobby> getHobbies() {
        return hobbies;
    }
}
