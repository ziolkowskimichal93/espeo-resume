package me.michalziolkowski.resume.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by michal.ziolkowski on 13.11.2017.
 */

public class Project implements Parcelable {

    private String name;
    private String description;
    private String logoUrl;
    private List<Skill> skills;
    private List<Screenshot> screenshots;

    public Project(String name, String description, List<Skill> skills, List<Screenshot> screenshots, String logoUrl) {
        this.name = name;
        this.description = description;
        this.logoUrl = logoUrl;
        this.skills = skills;
        this.screenshots = screenshots;
    }

    protected Project(Parcel in) {
        name = in.readString();
        description = in.readString();
        logoUrl = in.readString();
        skills = in.createTypedArrayList(Skill.CREATOR);
        screenshots = in.createTypedArrayList(Screenshot.CREATOR);
    }

    public static final Creator<Project> CREATOR = new Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel in) {
            return new Project(in);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getDescription() {
        return description;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public List<Screenshot> getScreenshots() {
        return screenshots;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(logoUrl);
        dest.writeTypedList(skills);
        dest.writeTypedList(screenshots);
    }
}
