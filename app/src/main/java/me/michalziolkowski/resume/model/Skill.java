package me.michalziolkowski.resume.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by michal.ziolkowski on 15.11.2017.
 */

public class Skill implements Parcelable{

    private String name;

    public Skill(String name) {
        this.name = name;
    }

    protected Skill(Parcel in) {
        name = in.readString();
    }

    public static final Creator<Skill> CREATOR = new Creator<Skill>() {
        @Override
        public Skill createFromParcel(Parcel in) {
            return new Skill(in);
        }

        @Override
        public Skill[] newArray(int size) {
            return new Skill[size];
        }
    };

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
