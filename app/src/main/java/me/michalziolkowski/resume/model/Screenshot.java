package me.michalziolkowski.resume.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by michal.ziolkowski on 15.11.2017.
 */

public class Screenshot implements Parcelable {

    private String url;

    public Screenshot(String url) {
        this.url = url;
    }

    protected Screenshot(Parcel in) {
        url = in.readString();
    }

    public static final Creator<Screenshot> CREATOR = new Creator<Screenshot>() {
        @Override
        public Screenshot createFromParcel(Parcel in) {
            return new Screenshot(in);
        }

        @Override
        public Screenshot[] newArray(int size) {
            return new Screenshot[size];
        }
    };

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
    }
}
