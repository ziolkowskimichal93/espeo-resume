package me.michalziolkowski.resume.model;

/**
 * Created by michal.ziolkowski on 14.11.2017.
 */

public class Hobby {
    private String name;
    private String iconUrl;

    public Hobby(String name, String iconUrl) {
        this.name = name;
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
