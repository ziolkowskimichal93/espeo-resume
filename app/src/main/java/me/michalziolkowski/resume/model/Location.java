package me.michalziolkowski.resume.model;

/**
 * Created by michal.ziolkowski on 16.11.2017.
 */

public class Location {

    private double latitude;

    private double longitude;

    private String name;

    public Location(double latitude, double longitude, String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }
}
