package me.michalziolkowski.resume.util;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Test;

import me.michalziolkowski.resume.util.DirectionUtils;

import static org.junit.Assert.assertEquals;

public class DirectionUtilsUnitTest {

    private static double TEST_LOCATION_1_LAT = 53.427833;
    private static double TEST_LOCATION_1_LNG = 14.538450;
    private static double TEST_LOCATION_2_LAT = 53.423563;
    private static double TEST_LOCATION_2_LNG = 14.574370;
    private static String TEST_LOCATIONS_URL = "https://maps.googleapis.com/maps/api/directions/json?origin="
        + TEST_LOCATION_1_LAT + "," + TEST_LOCATION_1_LNG + "&destination="+ TEST_LOCATION_2_LAT +"," + TEST_LOCATION_2_LNG +
            "&sensor=false";
    private static double TEST_DISTANCE_KM = 1024;
    private static double TEST_DISTANCE_M = 24;
    private static String TEST_DISTANCE_KM_TEXT = "1,0 km";
    private static String TEST_DISTANCE_M_TEXT = "24 m";

    @Test
    public void testGetDirectionsUrl() throws Exception {
        String directionsUrl = DirectionUtils.getDirectionsUrl(new LatLng(TEST_LOCATION_1_LAT, TEST_LOCATION_1_LNG),
                new LatLng(TEST_LOCATION_2_LAT, TEST_LOCATION_2_LNG));
        assertEquals(TEST_LOCATIONS_URL, directionsUrl);
    }

    @Test
    public void testGetDistanceTextMeters() throws Exception {
        String distanceText = DirectionUtils.getDistanceText(TEST_DISTANCE_M);
        assertEquals(TEST_DISTANCE_M_TEXT, distanceText);
    }

    @Test
    public void testGetDistanceTextKilometers() throws Exception {
        String distanceText = DirectionUtils.getDistanceText(TEST_DISTANCE_KM);
        assertEquals(TEST_DISTANCE_KM_TEXT, distanceText);
    }
}