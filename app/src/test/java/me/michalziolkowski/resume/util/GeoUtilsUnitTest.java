package me.michalziolkowski.resume.util;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Test;

import me.michalziolkowski.resume.util.GeoUtils;
import me.michalziolkowski.resume.util.JsonUtils;

import static org.junit.Assert.*;

public class GeoUtilsUnitTest {

    private static LatLng TEST_LOCATION_1 = new LatLng(53.427833, 14.538450);
    private static LatLng TEST_LOCATION_2 = new LatLng(53.423563, 14.574370);
    private static double TEST_LOCATIONS_DISTANCE = 3073;

    @Test
    public void testDistanceCalculation() throws Exception {
        double distance = GeoUtils.getDistance(TEST_LOCATION_1,TEST_LOCATION_2);
        assertEquals(TEST_LOCATIONS_DISTANCE, distance, 1);
    }
}